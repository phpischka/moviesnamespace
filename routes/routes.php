<?php

/* 
 * $1 - Parameters from REQUEST URL
 * ([0-9]+) - решулярное выражение  соответствует цифрам
 */
return  
    [
        //Directors
        'director/([0-9]+)/delete' => 'DirectorController/actionDelete/$1',
        'director/([0-9]+)/edit' => 'DirectorController/actionUpdate/$1',
        'director/create' => 'DirectorController/actionCreate',
        'directors/page-([0-9]+)' => 'DirectorController/actionIndex/$1',
        'directors' => 'DirectorController/actionIndex',
        //Movies
        'movie/([0-9]+)/delete' => 'MovieController/actionDelete/$1',
        'movie/([0-9]+)/edit' => 'MovieController/actionUpdate/$1',
        'movie/create' => 'MovieController/actionCreate',
        'movies/page-([0-9]+)' => 'MovieController/actionIndex/$1',
        'movies' => 'MovieController/actionIndex',
        //Login 
        'login' => 'UserController/actionLogin',
        'logout' => 'UserController/actionLogout',
        //Home
        '' => 'HomeController/actionIndex',
    ];
