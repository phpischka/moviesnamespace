<?php
namespace Components;
use \Controllers;
/**
 * Класс Router
 * Компонент для работы с маршрутами
 */
class Router {
    
    private $routes;
    
    public function __construct() {
        $routesPath = ROOT . '/routes/routes.php';
        $this->routes = include($routesPath);
    }
    
    /**
     * returns request string
     * @return type string
     */
    private function getUri() {
        $request = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_STRING);
        if (!empty($request)) {
            return trim($request, '/');
        }
    }
    
    public function run() {
        //Получить строку запроса
        $uri = $this->getUri();
        $result = null;
        
        //Проверить наличие такого запроса в routes.php
        foreach ($this->routes as $uriPattern => $path){
            //Если есть совпадение определить какой контроллер и 
            //метод action обрабатывает запрос, параметры
            if (preg_match("~^$uriPattern$~", $uri)) {
                // Получаем внутренний путь из внешнего согласно правилу.
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                
                $segments = explode('/', $internalRoute);
                $controllerName = array_shift($segments);
                $actionName = array_shift($segments);
                $parameters = $segments;
                
                //Получить обьект вызвать метод action
                $controllerName = "\\Controllers\\" . $controllerName;
                
                $controllerObject = new $controllerName;
                
                /* Вызываем необходимый метод ($actionName) у определенного 
                 * класса ($controllerObject) с заданными ($parameters) параметрами
                 */
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                
                if ($result != null){
                    break;
                }
            }
           
        }
        if (!$result) {
            header("HTTP/1.0 404 Not Found");
        }
    }
}
