
<?php include ROOT . '/views/layouts/header.php'; ?>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Add Director</div>

                            <div class="card-body">

                                <form action="/director/create" method="post" enctype="multipart/form-data">
                                    
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" class="form-control" id="name"  value="<?php echo $_SESSION['fields_director']['name'] ?? '' ?>" placeholder="Enter Name">
                                        <?php if (isset($errors['name'])): ?>
                                            <small class="form-text text-muted"><?php echo $errors['name']; ?></small>
                                        <?php endif; ?>
                                    </div>
                                    
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>     
    
<?php include ROOT . '/views/layouts/footer.php'; ?>


