
<?php include ROOT . '/views/layouts/header.php'; ?>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Directors</div>

                            <div class="card-body">
                                <a href="/director/create" class="btn btn-success mb-3" role="button" aria-disabled="true">Add Director</a>
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">Name</th>
                                          <th scope="col" class="actions">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($directors as $director): ?>
                                          <tr>
                                            <th scope="row"><?php echo $director['id'];?></th>
                                            <td><?php echo $director['name'];?></td>
                                            <td class="actions">
                                                <div class="btn-group">
                                                    <a href="/director/<?php echo $director['id'];?>/edit" class="btn btn-primary m-1" role="button" aria-disabled="true">Edit</a>
                                                    <a href="/director/<?php echo $director['id'];?>/delete" class="btn btn-danger m-1" role="button" aria-disabled="true">Delete</a>
                                                </div>
                                            </td>
                                          </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <!-- Постраничная навигация -->
                                <?php echo $pagination->get(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>     
    
<?php include ROOT . '/views/layouts/footer.php'; ?>


