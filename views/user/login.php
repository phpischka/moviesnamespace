
<?php include ROOT . '/views/layouts/header.php'; ?>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-header">LOGIN</div>

                            <div class="card-body">
                                <?php if (isset($errors['status'])): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?php echo $errors['status']; ?>
                                    </div>
                                <?php endif; ?>
                                <form action="login" method="post" enctype="multipart/form-data">
                                   
                                    <div class="form-group">
                                        <label for="login">login</label>
                                        <input type="text" name="login" class="form-control" id="login" placeholder="Enter Name">
                                        <?php if (isset($errors['login'])): ?>
                                            <small class="form-text text-muted"><?php echo $errors['login']; ?></small>
                                        <?php endif; ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" class="form-control" id="password">
                                        <?php if (isset($errors['password'])): ?>
                                            <small class="form-text text-muted"><?php echo $errors['password']; ?></small>
                                        <?php endif; ?>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>     
    
<?php include ROOT . '/views/layouts/footer.php'; ?>


