
<?php include ROOT . '/views/layouts/header.php'; ?>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Add Movie</div>

                            <div class="card-body">

                                <form action="/movie/create" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="name">Director</label>
                                        <select id="directorId" name="directorId" class="custom-select">
                                            <option value="">Select</option>
                                            <?php foreach ($directors as $director): ?>
                                                <?php if (isset($_SESSION['fields']['directorId']) && $_SESSION['fields']['directorId'] == $director['id']): ?>
                                                    <option selected="" value="<?php echo $director['id'];?>">
                                                        <?php echo $director['name'];?>
                                                    </option>
                                                <?php else: ?>
                                                    <option value="<?php echo $director['id'];?>">
                                                        <?php echo $director['name'];?>
                                                    </option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                        <?php if (isset($errors['directorId'])): ?>
                                            <small class="form-text text-muted"><?php echo $errors['directorId']; ?></small>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" class="form-control" id="name"  value="<?php echo $_SESSION['fields']['name'] ?? '' ?>" placeholder="Enter Name">
                                        <?php if (isset($errors['name'])): ?>
                                            <small class="form-text text-muted"><?php echo $errors['name']; ?></small>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" class="form-control" id="description" rows="3" placeholder="Enter description"><?php echo $_SESSION['fields']['description'] ?? '' ?></textarea>
                                        <?php if (isset($errors['description'])): ?>
                                            <small class="form-text text-muted"><?php echo $errors['description']; ?></small>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="releaseDate">Release Date</label>
                                        <input type="date" name="releaseDate" value="<?php echo $_SESSION['fields']['releaseDate'] ?? '' ?>" class="form-control" id="releaseDate">
                                        <?php if (isset($errors['releaseDate'])): ?>
                                            <small class="form-text text-muted"><?php echo $errors['releaseDate']; ?></small>
                                        <?php endif; ?>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>     
    
<?php include ROOT . '/views/layouts/footer.php'; ?>


