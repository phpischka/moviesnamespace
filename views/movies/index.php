
<?php include ROOT . '/views/layouts/header.php'; ?>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Movies</div>

                            <div class="card-body">

                                <a href="/movie/create" class="btn btn-success mb-3" role="button" aria-disabled="true">Add Movie</a>
                                <table class="table table-responsive">
                                    <thead class="thead-light">
                                    <tr>
                                      <th scope="col">#</th>
                                      <th scope="col">Director</th>
                                      <th scope="col">Name</th>
                                      <th scope="col">Description</th>
                                      <th scope="col">Release Date</th>
                                      <th scope="col">Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach ($movies as $movie): ?>
                                    <tr>
                                      <th scope="row"><?php echo $movie['id'];?></th>
                                      <td><?php echo $movie['directorName'];?></td>
                                      <td><?php echo $movie['movieName'];?></td>
                                      <td><?php echo $movie['description'];?></td>
                                      <td><?php echo $movie['releaseDate'];?></td>
                                      <td>
                                          <a href="/movie/<?php echo $movie['id'];?>/edit" class="btn btn-primary btn-block mb-1" role="button" aria-disabled="true">Edit</a>
                                          <a href="/movie/<?php echo $movie['id'];?>/delete" class="btn btn-danger btn-block mb-1" role="button" aria-disabled="true">Delete</a>
                                      </td>
                                    </tr>
                                  <?php endforeach; ?>
                                  </tbody>
                                </table>
                                <!-- Постраничная навигация -->
                                <?php echo $pagination->get(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>     
    
<?php include ROOT . '/views/layouts/footer.php'; ?>


